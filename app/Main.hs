module Main where

import Lib
import qualified Demo1
import qualified Demo2
import qualified Demo3
import qualified Demo4

main :: IO ()
main = putStrLn "hi"


main' :: IO ()
main' = do
  Demo1.main
  Demo2.main
  Demo3.main
  Demo4.main
