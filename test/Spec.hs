{-# LANGUAGE QuasiQuotes       #-}

module Main where

import Test.Hspec
import qualified Text.XML.Cursor as C
import qualified Text.XML as X
import Text.RawString.QQ (r)
import qualified Lib as L
import qualified Data.Map as M

main :: IO ()
main = do
  hspec $ do
    it "metas" $ do
      let txt = [r|
                  <post foo="bar"
                        baz="123">
                  </post>
                  |]
      let (Right doc) = X.parseText X.def txt
      L.metas doc `shouldBe` (M.fromList
                              [ ("foo", "bar")
                              , ("baz", "123")
                              ])
    it "aliases" $ do
      let foo = [r|
                  <post>
                    <aliases>
                      <alias>alias1</alias>
                      <alias>alias2</alias>
                    </aliases>
                  </post>
                  |]
      let (Right doc) = X.parseText X.def foo
      L.aliases doc `shouldBe` ["alias1", "alias2"]

    it "full example" $ do
      doc <- X.readFile X.def "test/fixtures/sample-post.xml"
      let aliases = L.aliases doc
      L.metas doc `shouldBe` (M.fromList
                              [ ("pub-date","2019-03-13")
                              , ("pub-state","draft")
                              , ("type","entry")
                              ])
      L.aliases doc `shouldBe` ["jargon"]

    it "render" $ do
       doc <- X.readFile X.def "test/fixtures/sample-post.xml"
       -- TODO add some comments
       L.render doc `seq` (() `shouldBe` ())
