{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE QuasiQuotes       #-}

module Lib where

import Prelude hiding (readFile)
import qualified Text.XML as X
import qualified Text.XML.Cursor as C
import qualified Data.Text as T
import qualified Data.Map as M
import Data.Maybe (catMaybes)
import           Text.Hamlet.XML (xml)
import           Text.Blaze.Html.Renderer.String (renderHtml)
import           Text.Blaze.Html                 (toHtml)

type Metas = M.Map T.Text T.Text

nodeAttributes :: X.Node -> (Maybe (M.Map X.Name T.Text))
nodeAttributes =
  \case
    X.NodeElement e -> Just $ X.elementAttributes e
    _ -> Nothing

render :: X.Document -> String
render doc =
  let
    cursor = C.fromDocument doc
    node = C.node cursor
  in
    renderHtml $ toHtml $ document $ root node

root :: X.Node -> X.Element
root node =
  let
    foo =
      case node of
        X.NodeElement (X.Element _name _attrs children) -> children
        _ -> []
  in
    X.Element "html" M.empty [xml|
<head>
    <title>Test
    <style>body > h1 { color: red }
<body>
    <h1>Hello World!
    $forall child <- foo
      ^{goNode child}
|]

document :: X.Element -> X.Document
document root = X.Document (X.Prologue [] Nothing []) root []

goNode :: X.Node -> [X.Node]
goNode (X.NodeElement e) = X.NodeElement <$> goElem e
goNode (X.NodeContent t) = [X.NodeContent t]
goNode (X.NodeComment _) = [] -- hide comments
goNode (X.NodeInstruction _) = [] -- and hide processing instructions too

-- convert each source element to its XHTML equivalent
goElem :: X.Element -> [X.Element]
goElem (X.Element "todo" attrs children) = [] -- do not include any todos in output
goElem (X.Element "aliases" attrs children) = [] -- do not include any aliases in output
goElem (X.Element "para" attrs children) =
    pure $ X.Element "p" attrs $ concatMap goNode children
goElem (X.Element "em" attrs children) =
    pure $ X.Element "i" attrs $ concatMap goNode children
goElem (X.Element "strong" attrs children) =
    pure $ X.Element "b" attrs $ concatMap goNode children
goElem (X.Element "image" attrs _children) =
    pure $ X.Element "img" (fixAttr attrs) [] -- images can't have children
  where
    fixAttr mattrs
        | "href" `M.member` mattrs  = M.delete "href" $ M.insert "src" (mattrs M.! "href") mattrs
        | otherwise                 = mattrs
goElem (X.Element name attrs children) =
    -- don't know what to do, just pass it through...
    pure $ X.Element name attrs $ concatMap goNode children

metas :: X.Document -> M.Map X.Name T.Text
metas doc =
  let
    root = X.documentRoot doc
  in
    X.elementAttributes root

aliases :: X.Document -> [T.Text]
aliases doc = do
  let cursor = C.fromDocument doc
  let aliases = C.child cursor >>= C.element "aliases" >>=
                C.child >>= C.element "alias" >>= C.child >>= C.content
  aliases
